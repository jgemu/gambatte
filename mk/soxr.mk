DIR_SOXR := $(wildcard $(DEPDIR)/soxr)

ifneq ($(DIR_SOXR),)
	USE_VENDORED_SOXR ?= 0
else
	override USE_VENDORED_SOXR := 0
endif

ifneq ($(USE_VENDORED_SOXR), 0)
	CFLAGS_SOXR := -I$(DIR_SOXR)
	LIBS_SOXR :=
	MKDIRS += deps/soxr
	SRCS_SOXR := deps/soxr/data-io.c \
		deps/soxr/dbesi0.c \
		deps/soxr/fft4g.c \
		deps/soxr/fft4g32.c \
		deps/soxr/fft4g64.c \
		deps/soxr/filter.c \
		deps/soxr/soxr.c \
		deps/soxr/vr32.c
	OBJS_SOXR := $(patsubst %,$(OBJDIR)/%,$(SRCS_SOXR:.c=.o))
else
	CFLAGS_SOXR = $(shell $(PKG_CONFIG) --cflags soxr)
	LIBS_SOXR = $(shell $(PKG_CONFIG) --libs soxr)
	OBJS_SOXR :=
endif

ifneq (,$(and $(findstring soxr,$(LIBS_REQUIRES)), \
		$(filter 0,$(USE_VENDORED_SOXR))))
	override REQUIRES_PRIVATE += soxr
endif

ifneq ($(DIR_SOXR),)
FLAGS_SOXR := -std=c99
DEFINES_SOXR := -DSOXR_LIB

ifneq ($(PLATFORM), Windows)
	FLAGS_SOXR += -fvisibility=hidden
endif

BUILD_SOXR = $(call COMPILE_C, $(FLAGS_SOXR) $(DEFINES_SOXR))

$(OBJDIR)/deps/soxr/%.o: $(DIR_SOXR)/%.c $(OBJDIR)/.tag
	$(call COMPILE_INFO,$(BUILD_SOXR))
	@$(BUILD_SOXR)

ifneq ($(USE_VENDORED_SOXR), 0)
install-docs::
	cp $(DIR_SOXR)/LICENSE $(DESTDIR)$(DOCDIR)/LICENSE-soxr
endif
endif
