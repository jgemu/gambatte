//
//   Copyright (C) 2010 by sinamas <sinamas at users.sourceforge.net>
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 2 as
//   published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License version 2 for more details.
//
//   You should have received a copy of the GNU General Public License
//   version 2 along with this program; if not, write to the
//   Free Software Foundation, Inc.,
//   51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA.
//

#ifndef INTERRUPT_REQUESTER_H
#define INTERRUPT_REQUESTER_H

#include "counterdef.h"
#include "minkeeper.h"

namespace gambatte {

struct SaveState;

enum IntEventId { intevent_unhalt,
                  intevent_end,
                  intevent_blit,
                  intevent_serial,
                  intevent_oam,
                  intevent_dma,
                  intevent_tima,
                  intevent_video,
                  intevent_interrupts, intevent_last = intevent_interrupts };

class InterruptRequester {
public:
	InterruptRequester();
	void saveState(SaveState &) const;
	void loadState(SaveState const &);
	void resetCc(unsigned long, unsigned long);
	unsigned ifreg() const;
	unsigned pendingIrqs() const;
	bool ime() const;
	bool halted() const;
	void ei(unsigned long);
	void di();
	void halt();
	void unhalt();
	void flagIrq(unsigned);
	void flagIrq(unsigned, unsigned long);
	void ackIrq(unsigned);
	void setIereg(unsigned);
	void setIfreg(unsigned);
	void setMinIntTime(unsigned long);

	IntEventId minEventId() const;
	unsigned long minEventTime() const;
	template<IntEventId> void setEventTime(unsigned long);
	void setEventTime(IntEventId, unsigned long);
	unsigned long eventTime(IntEventId) const;

private:
	class IntFlags {
	public:
		IntFlags() : flags_(0) {}
		bool ime() const;
		bool halted() const;
		bool imeOrHalted() const;
		void setIme();
		void unsetIme();
		void setHalted();
		void unsetHalted();
		void set(bool, bool);

	private:
		unsigned char flags_;
		enum { flag_ime = 1, flag_halted = 2 };
	};

	MinKeeper<intevent_last + 1> eventTimes_;
	unsigned long minIntTime_;
	unsigned ifreg_;
	unsigned iereg_;
	IntFlags intFlags_;
};

template<IntEventId id>
void InterruptRequester::setEventTime(unsigned long value) {
	eventTimes_.setValue<id>(value);
}

inline void flagHdmaReq(InterruptRequester &intreq) {
	intreq.setEventTime<intevent_dma>(0);
}

inline void flagGdmaReq(InterruptRequester &intreq) {
	intreq.setEventTime<intevent_dma>(1);
}

inline void ackDmaReq(InterruptRequester &intreq) {
	intreq.setEventTime<intevent_dma>(disabled_time);
}

inline bool hdmaReqFlagged(InterruptRequester const &intreq) {
	return intreq.eventTime(intevent_dma) == 0;
}

inline bool gdmaReqFlagged(InterruptRequester const &intreq) {
	return intreq.eventTime(intevent_dma) == 1;
}

}

#endif
