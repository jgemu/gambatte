//
//   Copyright (C) 2007 by sinamas <sinamas at users.sourceforge.net>
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 2 as
//   published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License version 2 for more details.
//
//   You should have received a copy of the GNU General Public License
//   version 2 along with this program; if not, write to the
//   Free Software Foundation, Inc.,
//   51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA.
//

#ifndef TIMA_H
#define TIMA_H

#include "interruptrequester.h"

namespace gambatte {

class TimaInterruptRequester {
public:
	explicit TimaInterruptRequester(InterruptRequester &intreq) : intreq_(intreq) {}
	void flagIrq() const;
	void flagIrq(unsigned long) const;
	unsigned long nextIrqEventTime() const;
	void setNextIrqEventTime(unsigned long) const;

private:
	InterruptRequester &intreq_;
};

class Tima {
public:
	Tima();
	void saveState(SaveState&) const;
	void loadState(const SaveState&, TimaInterruptRequester);
	void resetCc(unsigned long, unsigned long, TimaInterruptRequester);
	void setTima(unsigned, unsigned long, TimaInterruptRequester);
	void setTma(unsigned, unsigned long, TimaInterruptRequester);
	void setTac(unsigned, unsigned long, TimaInterruptRequester);
	void divReset(unsigned long, TimaInterruptRequester);
	void speedChange(TimaInterruptRequester);
	unsigned long divLastUpdate() const;
	unsigned tima(unsigned long);
	void doIrqEvent(TimaInterruptRequester);

private:
	unsigned long divLastUpdate_;
	unsigned long lastUpdate_;
	unsigned long tmatime_;
	unsigned char tima_;
	unsigned char tma_;
	unsigned char tac_;

	void updateIrq(unsigned long const, TimaInterruptRequester);
	void updateTima(unsigned long);
};

}

#endif
