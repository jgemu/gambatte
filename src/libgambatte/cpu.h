//
//   Copyright (C) 2007 by sinamas <sinamas at users.sourceforge.net>
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 2 as
//   published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License version 2 for more details.
//
//   You should have received a copy of the GNU General Public License
//   version 2 along with this program; if not, write to the
//   Free Software Foundation, Inc.,
//   51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA.
//

#ifndef CPU_H
#define CPU_H

#include "memory.h"

namespace gambatte {

class File;

class CPU {
public:
	CPU();
	long runFor(unsigned long);
	void setStatePtrs(SaveState&);
	void saveState(SaveState&);
	void loadState(SaveState const&);
	void loadSavedata();
	void saveSavedata();

	void setVideoBuffer(uint_least32_t*, std::ptrdiff_t);
	void setInputGetter(InputGetter*);
	void setSaveDir(std::string const&);
	std::string const saveBasePath() const;
	void setOsdElement(transfer_ptr<OsdElement>);
	LoadRes load(File&, std::string const&, bool, bool);

	bool loaded() const;
	char const * romTitle() const;
	PakInfo const pakInfo(bool) const;
	void setSoundBuffer(uint_least32_t*);
	std::size_t fillSoundBuffer();
	bool isCgb() const;

	void setCgbColorCorrection(int);
	void setDmgPaletteColor(int, int, unsigned long);

	void setGameGenie(std::string const&);
	void setGameShark(std::string const&);

private:
	Memory mem_;
	unsigned long cycleCounter_;
	unsigned short pc_;
	unsigned short sp;
	unsigned hf1, hf2, zf, cf;
	unsigned char a_, b, c, d, e, /*f,*/ h, l;
	unsigned char opcode_;
	bool prefetched_;

	void process(unsigned long);
};

}

#endif
