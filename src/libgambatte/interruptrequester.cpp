//
//   Copyright (C) 2010 by sinamas <sinamas at users.sourceforge.net>
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 2 as
//   published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License version 2 for more details.
//
//   You should have received a copy of the GNU General Public License
//   version 2 along with this program; if not, write to the
//   Free Software Foundation, Inc.,
//   51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA.
//

#include "interruptrequester.h"
#include "savestate.h"

namespace gambatte {

InterruptRequester::InterruptRequester()
: eventTimes_(disabled_time)
, minIntTime_(0)
, ifreg_(0)
, iereg_(0)
{
}

void InterruptRequester::saveState(SaveState &state) const {
	state.mem.minIntTime = minIntTime_;
	state.mem.IME = ime();
	state.mem.halted = halted();
}

void InterruptRequester::loadState(SaveState const &state) {
	minIntTime_ = state.mem.minIntTime;
	ifreg_ = state.mem.ioamhram.get()[0x10F];
	iereg_ = state.mem.ioamhram.get()[0x1FF] & 0x1F;
	intFlags_.set(state.mem.IME, state.mem.halted);

	eventTimes_.setValue<intevent_interrupts>(intFlags_.imeOrHalted() && pendingIrqs()
		? minIntTime_
		: static_cast<unsigned long>(disabled_time));
}

void InterruptRequester::resetCc(unsigned long oldCc, unsigned long newCc) {
	minIntTime_ = minIntTime_ < oldCc ? 0 : minIntTime_ - (oldCc - newCc);

	if (eventTimes_.value(intevent_interrupts) != disabled_time)
		eventTimes_.setValue<intevent_interrupts>(minIntTime_);
}

unsigned InterruptRequester::ifreg() const {
	return ifreg_;
}

unsigned InterruptRequester::pendingIrqs() const {
	return ifreg_ & iereg_;
}

bool InterruptRequester::ime() const {
	return intFlags_.ime();
}

bool InterruptRequester::halted() const {
	return intFlags_.halted();
}

void InterruptRequester::ei(unsigned long cc) {
	intFlags_.setIme();
	minIntTime_ = cc + 1;

	if (pendingIrqs())
		eventTimes_.setValue<intevent_interrupts>(minIntTime_);
}

void InterruptRequester::di() {
	intFlags_.unsetIme();

	if (!intFlags_.imeOrHalted())
		eventTimes_.setValue<intevent_interrupts>(disabled_time);
}

void InterruptRequester::halt() {
	intFlags_.setHalted();

	if (pendingIrqs())
		eventTimes_.setValue<intevent_interrupts>(minIntTime_);
}

void InterruptRequester::unhalt() {
	intFlags_.unsetHalted();

	if (!intFlags_.imeOrHalted())
		eventTimes_.setValue<intevent_interrupts>(disabled_time);
}

void InterruptRequester::flagIrq(unsigned bit) {
	ifreg_ |= bit;

	if (intFlags_.imeOrHalted() && pendingIrqs())
		eventTimes_.setValue<intevent_interrupts>(minIntTime_);
}

void InterruptRequester::flagIrq(unsigned bit, unsigned long cc) {
	unsigned const prevPending = pendingIrqs();
	ifreg_ |= bit;

	if (!prevPending && pendingIrqs() && intFlags_.imeOrHalted()) {
		minIntTime_ = std::max(minIntTime_, cc);
		eventTimes_.setValue<intevent_interrupts>(minIntTime_);
	}
}

void InterruptRequester::ackIrq(unsigned bit) {
	ifreg_ &= ~bit;
}

void InterruptRequester::setIereg(unsigned iereg) {
	iereg_ = iereg & 0x1F;

	if (intFlags_.imeOrHalted()) {
		eventTimes_.setValue<intevent_interrupts>(pendingIrqs()
			? minIntTime_
			: static_cast<unsigned long>(disabled_time));
	}
}

void InterruptRequester::setIfreg(unsigned ifreg) {
	ifreg_ = ifreg;

	if (intFlags_.imeOrHalted()) {
		eventTimes_.setValue<intevent_interrupts>(pendingIrqs()
			? minIntTime_
			: static_cast<unsigned long>(disabled_time));
	}
}

void InterruptRequester::setMinIntTime(unsigned long cc) {
	minIntTime_ = cc;

	if (eventTimes_.value(intevent_interrupts) < minIntTime_)
		eventTimes_.setValue<intevent_interrupts>(minIntTime_);
}

IntEventId InterruptRequester::minEventId() const {
	return static_cast<IntEventId>(eventTimes_.min());
}

unsigned long InterruptRequester::minEventTime() const {
	return eventTimes_.minValue();
}

void InterruptRequester::setEventTime(IntEventId id, unsigned long value) {
	eventTimes_.setValue(id, value);
}

unsigned long InterruptRequester::eventTime(IntEventId id) const {
	return eventTimes_.value(id);
}

bool InterruptRequester::IntFlags::ime() const {
	return flags_ & flag_ime;
}

bool InterruptRequester::IntFlags::halted() const {
	return flags_ & flag_halted;
}

bool InterruptRequester::IntFlags::imeOrHalted() const {
	return flags_;
}

void InterruptRequester::IntFlags::setIme() {
	flags_ |= flag_ime;
}

void InterruptRequester::IntFlags::unsetIme() {
	flags_ &= ~(1u * flag_ime);
}

void InterruptRequester::IntFlags::setHalted() {
	flags_ |= flag_halted;
}

void InterruptRequester::IntFlags::unsetHalted() {
	flags_ &= ~(1u * flag_halted);
}

void InterruptRequester::IntFlags::set(bool ime, bool halted) {
	flags_ = halted * flag_halted + ime * flag_ime;
}

}
