//
//   Copyright (C) 2007 by sinamas <sinamas at users.sourceforge.net>
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 2 as
//   published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License version 2 for more details.
//
//   You should have received a copy of the GNU General Public License
//   version 2 along with this program; if not, write to the
//   Free Software Foundation, Inc.,
//   51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA.
//

#ifndef SOUND_H
#define SOUND_H

#include "sound/channel1.h"
#include "sound/channel2.h"
#include "sound/channel3.h"
#include "sound/channel4.h"

namespace gambatte {

class PSG {
public:
	PSG();
	void init(bool);
	void reset(bool);
	void divReset(bool);
	void setStatePtrs(SaveState&);
	void saveState(SaveState&);
	void loadState(SaveState const&);

	void generateSamples(unsigned long, bool);
	void resetCounter(unsigned long, unsigned long, bool);
	void speedChange(unsigned long, bool);
	std::size_t fillBuffer();
	void setBuffer(uint_least32_t*);

	bool isEnabled() const;
	void setEnabled(bool);

	void setNr10(unsigned);
	void setNr11(unsigned);
	void setNr12(unsigned);
	void setNr13(unsigned);
	void setNr14(unsigned, bool);

	void setNr21(unsigned);
	void setNr22(unsigned);
	void setNr23(unsigned);
	void setNr24(unsigned, bool);

	void setNr30(unsigned);
	void setNr31(unsigned);
	void setNr32(unsigned);
	void setNr33(unsigned);
	void setNr34(unsigned);
	unsigned waveRamRead(unsigned) const;
	void waveRamWrite(unsigned, unsigned);

	void setNr41(unsigned);
	void setNr42(unsigned);
	void setNr43(unsigned);
	void setNr44(unsigned);

	void setSoVolume(unsigned);
	void mapSo(unsigned);
	unsigned getStatus() const;

private:
	Channel1 ch1_;
	Channel2 ch2_;
	Channel3 ch3_;
	Channel4 ch4_;
	uint_least32_t *buffer_;
	std::size_t bufferPos_;
	unsigned long lastUpdate_;
	unsigned long cycleCounter_;
	unsigned long soVol_;
	uint_least32_t rsum_;
	bool enabled_;

	void accumulateChannels(unsigned long);
};

}

#endif
