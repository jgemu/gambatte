//
//   Copyright (C) 2007 by sinamas <sinamas at users.sourceforge.net>
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 2 as
//   published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License version 2 for more details.
//
//   You should have received a copy of the GNU General Public License
//   version 2 along with this program; if not, write to the
//   Free Software Foundation, Inc.,
//   51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA.
//

#ifndef SOUND_CHANNEL1_H
#define SOUND_CHANNEL1_H

#include <stdint.h>

#include "duty_unit.h"
#include "envelope_unit.h"
#include "length_counter.h"
#include "master_disabler.h"
#include "static_output_tester.h"

namespace gambatte {

struct SaveState;

class Channel1 {
public:
	Channel1();
	void setNr0(unsigned);
	void setNr1(unsigned, unsigned long);
	void setNr2(unsigned, unsigned long);
	void setNr3(unsigned, unsigned long);
	void setNr4(unsigned, unsigned long, unsigned long);
	void setSo(unsigned long, unsigned long);
	bool isActive() const;
	void update(uint_least32_t*, unsigned long, unsigned long, unsigned long);
	void reset();
	void resetCc(unsigned long, unsigned long);
	void init(bool);
	void saveState(SaveState&, unsigned long);
	void loadState(SaveState const&);

private:
	class SweepUnit : public SoundUnit {
	public:
		SweepUnit(MasterDisabler&, DutyUnit&);
		virtual void event();
		void nr0Change(unsigned);
		void nr4Init(unsigned long);
		void reset();
		void init(bool);
		void saveState(SaveState&) const;
		void loadState(SaveState const&);

	private:
		MasterDisabler &disableMaster_;
		DutyUnit &dutyUnit_;
		unsigned short shadow_;
		unsigned char nr0_;
		bool neg_;
		bool cgb_;

		unsigned calcFreq();
	};

	friend class StaticOutputTester<Channel1, DutyUnit>;

	StaticOutputTester<Channel1, DutyUnit> staticOutputTest_;
	DutyMasterDisabler disableMaster_;
	LengthCounter lengthCounter_;
	DutyUnit dutyUnit_;
	EnvelopeUnit envelopeUnit_;
	SweepUnit sweepUnit_;
	SoundUnit *nextEventUnit_;
	unsigned long soMask_;
	unsigned long prevOut_;
	unsigned char nr4_;
	bool master_;

	void setEvent();
};

}

#endif
