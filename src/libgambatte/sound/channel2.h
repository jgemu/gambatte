//
//   Copyright (C) 2007 by sinamas <sinamas at users.sourceforge.net>
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 2 as
//   published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License version 2 for more details.
//
//   You should have received a copy of the GNU General Public License
//   version 2 along with this program; if not, write to the
//   Free Software Foundation, Inc.,
//   51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA.
//

#ifndef SOUND_CHANNEL2_H
#define SOUND_CHANNEL2_H

#include <stdint.h>

#include "duty_unit.h"
#include "envelope_unit.h"
#include "length_counter.h"
#include "static_output_tester.h"

namespace gambatte {

struct SaveState;

class Channel2 {
public:
	Channel2();
	void setNr1(unsigned, unsigned long);
	void setNr2(unsigned, unsigned long);
	void setNr3(unsigned, unsigned long);
	void setNr4(unsigned, unsigned long, unsigned long);
	void setSo(unsigned long, unsigned long);
	bool isActive() const;
	void update(uint_least32_t*, unsigned long, unsigned long, unsigned long);
	void reset();
	void resetCc(unsigned long, unsigned long);
	void saveState(SaveState&, unsigned long);
	void loadState(SaveState const&);

private:
	friend class StaticOutputTester<Channel2, DutyUnit>;

	StaticOutputTester<Channel2, DutyUnit> staticOutputTest_;
	DutyMasterDisabler disableMaster_;
	LengthCounter lengthCounter_;
	DutyUnit dutyUnit_;
	EnvelopeUnit envelopeUnit_;
	SoundUnit *nextEventUnit;
	unsigned long soMask_;
	unsigned long prevOut_;
	unsigned char nr4_;
	bool master_;

	void setEvent();
};

}

#endif
