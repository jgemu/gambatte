//
//   Copyright (C) 2007 by sinamas <sinamas at users.sourceforge.net>
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 2 as
//   published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License version 2 for more details.
//
//   You should have received a copy of the GNU General Public License
//   version 2 along with this program; if not, write to the
//   Free Software Foundation, Inc.,
//   51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA.
//

#ifndef SOUND_CHANNEL4_H
#define SOUND_CHANNEL4_H

#include <stdint.h>

#include "envelope_unit.h"
#include "length_counter.h"
#include "master_disabler.h"
#include "static_output_tester.h"

namespace gambatte {

struct SaveState;

class Channel4 {
public:
	Channel4();
	void setNr1(unsigned, unsigned long);
	void setNr2(unsigned, unsigned long);
	void setNr3(unsigned, unsigned long);
	void setNr4(unsigned, unsigned long);
	void setSo(unsigned long, unsigned long);
	bool isActive() const;
	void update(uint_least32_t*, unsigned long, unsigned long, unsigned long);
	void reset(unsigned long);
	void resetCc(unsigned long, unsigned long);
	void saveState(SaveState&, unsigned long);
	void loadState(SaveState const&);

private:
	class Lfsr : public SoundUnit {
	public:
		Lfsr();
		virtual void event();
		virtual void resetCounters(unsigned long);
		bool isHighState() const;
		void nr3Change(unsigned, unsigned long);
		void nr4Init(unsigned long);
		void reset(unsigned long);
		void resetCc(unsigned long, unsigned long);
		void saveState(SaveState&, unsigned long);
		void loadState(SaveState const&);
		void disableMaster();
		void killCounter();
		void reviveCounter(unsigned long);

	private:
		unsigned long backupCounter_;
		unsigned short reg_;
		unsigned char nr3_;
		bool master_;

		void updateBackupCounter(unsigned long);
	};

	class Ch4MasterDisabler : public MasterDisabler {
	public:
		Ch4MasterDisabler(bool &m, Lfsr &lfsr) : MasterDisabler(m), lfsr_(lfsr) {}
		virtual void operator()();

	private:
		Lfsr &lfsr_;
	};

	friend class StaticOutputTester<Channel4, Lfsr>;

	StaticOutputTester<Channel4, Lfsr> staticOutputTest_;
	Ch4MasterDisabler disableMaster_;
	LengthCounter lengthCounter_;
	EnvelopeUnit envelopeUnit_;
	Lfsr lfsr_;
	SoundUnit *nextEventUnit_;
	unsigned long soMask_;
	unsigned long prevOut_;
	unsigned char nr4_;
	bool master_;

	void setEvent();
};

}

#endif
