SOURCEDIR := $(abspath $(patsubst %/,%,$(dir $(abspath $(lastword \
	$(MAKEFILE_LIST))))))

NAME := gambatte
JGNAME := $(NAME)-jg

DESCRIPTION := Gambatte JG is an emulator for the Nintendo Game Boy/Game Boy \
	Color.

SRCDIR := $(SOURCEDIR)/src

INCLUDES_BIN = -I$(SRCDIR)/libgambatte
INCLUDES_JG = -I$(SRCDIR)
INCLUDES = -I$(SRCDIR) -I$(SRCDIR)/libgambatte

LINKER = $(CXX)

LIBS = -lm
LIBS_STATIC = -lstdc++

LIBS_REQUIRES :=

DOCS := COPYING README
DOCS_EXAMPLE := README

EXAMPLE := src/example

HEADERS := src/libgambatte/gambatte.h \
	src/libgambatte/gambatte_inputgetter.h \
	src/libgambatte/gambatte_loadres.h

# Object dirs
MKDIRS := src/resample \
	src/libgambatte/file \
	src/libgambatte/mem \
	src/libgambatte/sound \
	src/libgambatte/video

# Global symbols
# TODO: Darwin export files expect mangled symbols for C++
SYMBOLS :=

define SYMBOLS_MAP
extern \"C++\" {\\
      gambatte::to_string*;\\
      gambatte::GB::*;\\
    };
endef

override INSTALL_DATA := 0
override INSTALL_EXAMPLE := 1
override INSTALL_SHARED := 1

include $(SOURCEDIR)/version.h
include $(SOURCEDIR)/mk/jg.mk

INCLUDES_JG += $(CFLAGS_SOXR)
LIBS_JG = $(LIBS_SOXR)

EXT := cpp
FLAGS := -std=c++98 $(WARNINGS_DEF_CXX)

# Example dependencies
INCLUDES_BIN += $(CFLAGS_SDL2) $(CFLAGS_SPEEXDSP)
LIBS_BIN = $(LIBS_SDL2) $(LIBS_SPEEXDSP)

CXXSRCS := src/libgambatte/bitmap_font.cpp \
	src/libgambatte/cpu.cpp \
	src/libgambatte/gambatte.cpp \
	src/libgambatte/gambatte_loadres.cpp \
	src/libgambatte/initstate.cpp \
	src/libgambatte/interrupter.cpp \
	src/libgambatte/interruptrequester.cpp \
	src/libgambatte/memory.cpp \
	src/libgambatte/sound.cpp \
	src/libgambatte/state_osd_elements.cpp \
	src/libgambatte/statesaver.cpp \
	src/libgambatte/tima.cpp \
	src/libgambatte/video.cpp \
	src/libgambatte/file/file.cpp \
	src/libgambatte/mem/cartridge.cpp \
	src/libgambatte/mem/memptrs.cpp \
	src/libgambatte/mem/pakinfo.cpp \
	src/libgambatte/mem/rtc.cpp \
	src/libgambatte/sound/channel1.cpp \
	src/libgambatte/sound/channel2.cpp \
	src/libgambatte/sound/channel3.cpp \
	src/libgambatte/sound/channel4.cpp \
	src/libgambatte/sound/duty_unit.cpp \
	src/libgambatte/sound/envelope_unit.cpp \
	src/libgambatte/sound/length_counter.cpp \
	src/libgambatte/video/ly_counter.cpp \
	src/libgambatte/video/lyc_irq.cpp \
	src/libgambatte/video/next_m0_time.cpp \
	src/libgambatte/video/ppu.cpp \
	src/libgambatte/video/sprite_mapper.cpp

BINSRCS := $(EXAMPLE)/example.cpp

JGSRCS := src/resample/chainresampler.cpp \
	src/resample/i0.cpp \
	src/resample/kaiser50sinc.cpp \
	src/resample/kaiser70sinc.cpp \
	src/resample/makesinckernel.cpp \
	src/resample/resamplerinfo.cpp \
	src/resample/u48div.cpp \
	jg.cpp

# List of object files
OBJS := $(patsubst %,$(OBJDIR)/%,$(CXXSRCS:.cpp=.o))
OBJS_BIN := $(patsubst %,$(OBJDIR)/%,$(BINSRCS:.cpp=.o))
OBJS_JG := $(patsubst %,$(OBJDIR)/%,$(JGSRCS:.cpp=.o)) $(OBJS_SOXR)

# Example commands
BUILD_EXAMPLE = $(call COMPILE_CXX, $(FLAGS) $(INCLUDES_BIN))

# Resample commands
BUILD_RESAMPLE = $(call COMPILE_CXX, $(FLAGS) $(INCLUDES_JG))

# Core commands
BUILD_JG = $(call COMPILE_CXX, $(FLAGS) $(INCLUDES_JG) $(CFLAGS_JG))
BUILD_MAIN = $(call COMPILE_CXX, $(FLAGS) $(INCLUDES))

.PHONY: $(PHONY)

all: $(TARGET)

# Resample rules
$(OBJDIR)/src/resample/%.o: $(SRCDIR)/resample/%.$(EXT) $(PREREQ)
	$(call COMPILE_INFO,$(BUILD_RESAMPLE))
	@$(BUILD_RESAMPLE)

# Core rules
$(OBJDIR)/src/%.o: $(SRCDIR)/%.$(EXT) $(PREREQ)
	$(call COMPILE_INFO,$(BUILD_MAIN))
	@$(BUILD_MAIN)

include $(SOURCEDIR)/mk/rules.mk
